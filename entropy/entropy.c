/*
 * entropy.c
 */

#include <stdio.h>
#include <stdlib.h>
#include "entropy.h"

/* entropy function
 *
 * THIS IS AN EXAMPLE FOR WORKING SAMPLE CODE ONLY.
 * IT DOES NOT SUPPLY REAL ENTROPY BECAUSE THE RANDOM SEED IS FIXED.
 *
 * IT SHOULD BE CHANGED SO THAT EACH COMMAND THAT REQUESTS A BYTE
 * OF ENTROPY RECEIVES A RANDOM BYTE.
 *
 * Returns 1 for success, 0 for failure.
 */

uint8_t get_entropy(ENTROPY_CMD cmd, uint8_t *out) {
	static uint8_t seed[SEED_SIZE];
	static size_t index;
	FILE *dev_random;
	size_t read_size;

	if (cmd == INIT) {
		printf("Gathering entropy... (if it appears to hang, try typing or "
				"moving the mouse cursor)");
		index = 0;
		dev_random = fopen("/dev/urandom", "r");
		read_size = fread(seed, sizeof(uint8_t), SEED_SIZE, dev_random);
		fclose(dev_random);
		return read_size == (SEED_SIZE * sizeof(uint8_t));
	}

	if (out == NULL)
		return 0;

	if (cmd == GET_NUM_BYTES_PER_BYTE_OF_ENTROPY) {
		/* Here we return the number of bytes needed from the entropy
		 * source to obtain 8 bits of entropy.  Maximum is 8.
		 */
		*out = 1; /* this is a perfectly random source */
		return 1;
	}

	if (cmd == GET_BYTE_OF_ENTROPY) {
		if (index == SEED_SIZE) {
			if (!get_entropy(INIT, NULL)) {
				return 0; /* used up all our entropy */
			}
		}

		*out = seed[index++]; /* deliver an entropy byte */
		return 1;
	}
	return 0;
}
