/*
 * entropy.h
 *
 *  Created on: Jan 13, 2014
 *      Author: scott
 */

#ifndef ENTROPY_H_
#define ENTROPY_H_

#include "ntru_crypto.h"

#define SEED_SIZE 48	/* ((strength / 8) * 1.5) */

uint8_t get_entropy(ENTROPY_CMD cmd, uint8_t *out);

#endif /* ENTROPY_H_ */
