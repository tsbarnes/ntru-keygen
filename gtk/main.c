/*
 * main.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <gio/gio.h>
#include <gtk/gtk.h>
#include "ntru_crypto.h"
#include "entropy.h"

#ifndef UI_DIR
#define UI_DIR "."
#endif

static GtkAssistant *assistant;
static GtkLabel *progress = NULL;
static GtkSpinner *spinner = NULL;
static GtkEntry *filename = NULL;
static GtkFileChooserButton *folder = NULL;

/* Personalization string to be used for DRBG instantiation.
 * This is optional.
 */
static uint8_t const pers_str[] = { 'N', 'T', 'R', 'U', 'V', 'P', 'N' };

uint8_t *public_key;
uint16_t public_key_len; /* no. of octets in public key */
uint8_t *private_key;
uint16_t private_key_len; /* no. of octets in private key */
uint8_t *encoded_public_key;
uint16_t encoded_public_key_len; /* no. of octets in encoded public key */

uint8_t gtk_get_entropy(ENTROPY_CMD cmd, uint8_t *out) {
	gtk_main_iteration();
	return get_entropy(cmd, out);
}

gboolean generate_keys() {
	DRBG_HANDLE drbg; /* handle for instantiated DRBG */
	uint32_t rc; /* return code */
	bool error = FALSE; /* records if error occurred */

	/* Instantiate a DRBG with 256-bit security strength for key generation
	 * to match the security strength of the EES1499EP1 parameter set.
	 * Here we've chosen to use the personalization string.
	 */
	rc = ntru_crypto_drbg_instantiate(256, pers_str, sizeof(pers_str),
			(ENTROPY_FN) &gtk_get_entropy, &drbg);
	if (rc != DRBG_OK) {
		switch (rc) {
		case DRBG_RESULT(DRBG_OUT_OF_MEMORY):
			printf("Couldn't allocate memory during DRBG instantiation.\n");
			break;
		case DRBG_RESULT(DRBG_BAD_PARAMETER):
			printf("Bad parameter given during DRBG instantiation.\n");
			break;
		case DRBG_RESULT(DRBG_BAD_LENGTH):
			printf("Invalid length given during DRBG instantiation.\n");
			break;
		case DRBG_RESULT(DRBG_NOT_AVAILABLE):
			printf("No slots available during DRBG instantiation.\n");
			break;
		case DRBG_RESULT(DRBG_ENTROPY_FAIL):
			printf("Entropy function error during DRBG instantiation.\n");
			break;
		default:
			printf("An error occurred during DRBG instantiation.\n");
			break;
		}
		return TRUE;
	}
	printf("DRBG at 256-bit security for key generation instantiated "
			"successfully.\n");

	gtk_main_iteration();

	/* Let's find out how large a buffer we need for the public and private
	 * keys.
	 */
	rc = ntru_crypto_ntru_encrypt_keygen(drbg, NTRU_EES1499EP1, &public_key_len,
	NULL, &private_key_len, NULL);
	if (rc != NTRU_OK) {
		/* An error occurred requesting the buffer sizes needed. */
		return TRUE;
	}
	printf("Public-key buffer size required: %d octets.\n", public_key_len);
	printf("Private-key buffer size required: %d octets.\n", private_key_len);

	public_key = malloc(sizeof(uint8_t) * public_key_len);
	private_key = malloc(sizeof(uint8_t) * private_key_len);

	gtk_main_iteration();

	/* Generate a key pair for EES401EP2.
	 * We must set the public-key length to the size of the buffer we have
	 * for the public key, and similarly for the private-key length.
	 * We've already done this by getting the sizes from the previous call
	 * to ntru_crypto_ntru_encrypt_keygen() above.
	 */
	uint16_t expected_private_key_len = private_key_len;
	rc = ntru_crypto_ntru_encrypt_keygen(drbg, NTRU_EES1499EP1, &public_key_len,
			public_key, &private_key_len, private_key);
	if (rc != NTRU_OK) {
		/* An error occurred during key generation. */
		error = TRUE;
	}
	if (expected_private_key_len != private_key_len) {
		fprintf(stderr, "private-key-length is different than expected\n");
		error = TRUE;
	}
	printf("Key-pair for NTRU_EES1499EP1 generated successfully.\n");

	gtk_main_iteration();

	/* Uninstantiate the DRBG. */
	rc = ntru_crypto_drbg_uninstantiate(drbg);
	if ((rc != DRBG_OK) || error) {
		printf("An error occurred uninstantiating the DRBG, or generating "
				"keys.\n");
		return TRUE;
	}
	printf("Key-generation DRBG uninstantiated successfully.\n");

	gtk_main_iteration();

	/* Let's find out how large a buffer we need for holding a DER-encoding
	 * of the public key.
	 */
	rc = ntru_crypto_ntru_encrypt_publicKey2SubjectPublicKeyInfo(public_key_len,
			public_key, &encoded_public_key_len, NULL);
	if (rc != NTRU_OK) {
		printf("An error occurred requesting the buffer size needed.\n");
		return TRUE;
	}
	printf("DER-encoded public-key buffer size required: %d octets.\n",
			encoded_public_key_len);

	encoded_public_key = malloc(sizeof(uint8_t) * encoded_public_key_len);
	uint16_t expected_encoded_public_key_len = encoded_public_key_len;

	gtk_main_iteration();

	/* DER-encode the public key for inclusion in a certificate.
	 * This creates a SubjectPublicKeyInfo field from a public key.
	 * We must set the encoded public-key length to the size of the buffer
	 * we have for the encoded public key.
	 * We've already done this by getting the size from the previous call
	 * to ntru_crypto_ntru_encrypt_publicKey2SubjectPublicKey() above.
	 */
	rc = ntru_crypto_ntru_encrypt_publicKey2SubjectPublicKeyInfo(public_key_len,
			public_key, &encoded_public_key_len, encoded_public_key);

	if (expected_encoded_public_key_len != encoded_public_key_len) {
		fprintf(stderr, "encoded_public_key_len is different than expected\n");
		error = TRUE;
	}

	printf("Public key DER-encoded for SubjectPublicKeyInfo successfully.\n");

	gtk_main_iteration();

	return error;
}

static gboolean write_to_file(GFile *file) {
	GError *error;
	gsize bytes_written;
	GFileOutputStream *output;

	output = g_file_replace(file, NULL, FALSE,
			G_FILE_CREATE_REPLACE_DESTINATION, NULL, &error);
	if (!output) {
		return FALSE;
	}

	if (!g_output_stream_write_all(G_OUTPUT_STREAM(output), (void*) private_key,
			private_key_len, &bytes_written, NULL, &error)) {
		return FALSE;
	}
	if (!g_output_stream_close(G_OUTPUT_STREAM(output), NULL, &error)) {
		return FALSE;
	}
	return TRUE;
}

static gboolean save() {
	gchar *folder_uri = gtk_file_chooser_get_uri(GTK_FILE_CHOOSER(folder));
	const gchar *filename_str = gtk_entry_get_text(filename);
	gsize uri_length = strlen(folder_uri) + strlen(filename_str) + 6;
	gchar *uri = g_malloc(uri_length);
	GFile *private_key_file;
	GFile *public_key_file;
	GFile *encoded_public_key_file;

	/* Writing both private key and public key to files */
	g_sprintf(uri, "%s/%s.key", folder_uri, gtk_entry_get_text(filename));
	private_key_file = g_file_new_for_uri(uri);
	g_printf("Writing private key to %s\n", uri);

	g_sprintf(uri, "%s/%s.pub", folder_uri, gtk_entry_get_text(filename));
	public_key_file = g_file_new_for_uri(uri);
	g_printf("Writing public key to %s\n", uri);

	g_sprintf(uri, "%s/%s.der", folder_uri, gtk_entry_get_text(filename));
	encoded_public_key_file = g_file_new_for_uri(uri);
	g_printf("Writing DER encoded public key to %s\n", uri);

	if (!private_key_file || !public_key_file || !encoded_public_key_file) {
		return FALSE;
	}

	if (!write_to_file(private_key_file)) {
		return FALSE;
	}
	if (!write_to_file(public_key_file)) {
		return FALSE;
	}
	if (!write_to_file(encoded_public_key_file)) {
		return FALSE;
	}

	return TRUE;
}

static void prepare_callback(GtkAssistant *_assistant, GtkWidget *page,
		gpointer data) {
	switch (gtk_assistant_get_current_page(assistant)) {
	case 1:
		gtk_assistant_commit(assistant);
		gtk_spinner_start(spinner);
		break;
	case 2:
		gtk_assistant_commit(assistant);
		break;
	case 3:
		gtk_assistant_commit(assistant);
		save();
		break;
	default:
		break;
	}
}

static void progress_callback(GtkWidget *widget, gpointer data) {
	GtkWidget *page = gtk_assistant_get_nth_page(assistant,
			gtk_assistant_get_current_page(assistant));
	generate_keys();
	gtk_spinner_stop(spinner);
	gtk_label_set_text(progress, "Finished generating keys.");
	gtk_assistant_set_page_complete(assistant, page, TRUE);
}

static void confirm_callback(GtkWidget *widget, gpointer data) {
	if (strlen(gtk_entry_get_text(filename)) > 0
			&& gtk_file_chooser_get_uri(GTK_FILE_CHOOSER(folder))) {
		gtk_assistant_set_page_complete(assistant,
				gtk_assistant_get_nth_page(assistant,
						gtk_assistant_get_current_page(assistant)), TRUE);
	}
}

static void close_callback(GtkAssistant *_assistant, gpointer data) {
	gtk_main_quit();
}

int main(int argc, char *argv[]) {
	GtkBuilder *builder;
	GFile *builder_ui;

	gtk_init(&argc, &argv);

	/* Construct a GtkBuilder instance and load our UI description */
	builder = gtk_builder_new();
	builder_ui = g_file_new_for_path("ntru-keygen.ui");
	if (!g_file_query_exists(builder_ui, NULL)) {
		g_object_unref(G_OBJECT(builder_ui));
		builder_ui = g_file_new_for_path(UI_DIR "/ntru-keygen.ui");
	}
	if (!g_file_query_exists(builder_ui, NULL)) {
		g_printf("Couldn't find " UI_DIR "/ntru-keygen.ui!  Aborting...\n");
		return 1;
	}
	g_printf("UI File: %s\n", g_file_get_path(builder_ui));

	gtk_builder_add_from_file(builder, g_file_get_path(builder_ui), NULL);
	spinner = GTK_SPINNER(gtk_builder_get_object(builder, "progress-spinner"));
	progress = GTK_LABEL(gtk_builder_get_object(builder, "progress-label"));
	filename = GTK_ENTRY(gtk_builder_get_object(builder, "filename-entry"));
	folder = GTK_FILE_CHOOSER_BUTTON(
			gtk_builder_get_object(builder, "folderchooser-button"));

	/* Connect signal handlers to the constructed widgets. */
	assistant = GTK_ASSISTANT(gtk_builder_get_object(builder, "assistant"));
	g_signal_connect(G_OBJECT(assistant), "destroy", G_CALLBACK(gtk_main_quit),
			NULL);
	g_signal_connect(G_OBJECT(assistant), "close", G_CALLBACK(close_callback),
			NULL);
	g_signal_connect(G_OBJECT(assistant), "cancel", G_CALLBACK(close_callback),
			NULL);
	g_signal_connect(G_OBJECT(assistant), "prepare",
			G_CALLBACK(prepare_callback), NULL);
	g_signal_connect(G_OBJECT(spinner), "realize",
			G_CALLBACK(progress_callback), NULL);
	g_signal_connect(G_OBJECT(filename), "changed",
			G_CALLBACK(confirm_callback), NULL);
	g_signal_connect(G_OBJECT(folder), "file-set", G_CALLBACK(confirm_callback),
			NULL);

	gtk_widget_show_all(GTK_WIDGET(assistant));

	gtk_main();

	return 0;
}
