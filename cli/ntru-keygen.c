/*
 * ntruvpn-keygen.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ntru_crypto.h"
#include "entropy.h"

/* Personalization string to be used for DRBG instantiation.
 * This is optional.
 */
static uint8_t const pers_str[] = { 'N', 'T', 'R', 'U', 'V', 'P', 'N' };

/* Dumps a buffer in hex to the screen for debugging */
void DumpHex(const unsigned char* buf, int len) {
	int i;
	for (i = 0; i < len; i++) {
		if (i & 0x1f)
			printf(":");
		printf("%02X", buf[i]);
		if ((i & 0x1f) == 0x1f)
			printf("\n");
	}
	printf("\n");
}

int main(void) {
	uint8_t *public_key;
	uint16_t public_key_len; /* no. of octets in public key */
	uint8_t *private_key;
	uint16_t private_key_len; /* no. of octets in private key */
	uint8_t *encoded_public_key;
	uint16_t encoded_public_key_len; /* no. of octets in encoded public key */
	uint8_t *next = NULL; /* points to next cert field to parse */
	DRBG_HANDLE drbg; /* handle for instantiated DRBG */
	uint32_t rc; /* return code */
	bool error = FALSE; /* records if error occurred */
	FILE *Handle = NULL; /* File Handle for writing NTRU key to file */

	/* Instantiate a DRBG with 256-bit security strength for key generation
	 * to match the security strength of the EES1499EP1 parameter set.
	 * Here we've chosen to use the personalization string.
	 */
	rc = ntru_crypto_drbg_instantiate(256, pers_str, sizeof(pers_str),
			(ENTROPY_FN) &get_entropy, &drbg);
	if (rc != DRBG_OK) {
		switch (rc) {
		case DRBG_RESULT(DRBG_OUT_OF_MEMORY):
			printf("Couldn't allocate memory during DRBG instantiation.\n");
			break;
		case DRBG_RESULT(DRBG_BAD_PARAMETER):
			printf("Bad parameter given during DRBG instantiation.\n");
			break;
		case DRBG_RESULT(DRBG_BAD_LENGTH):
			printf("Invalid length given during DRBG instantiation.\n");
			break;
		case DRBG_RESULT(DRBG_NOT_AVAILABLE):
			printf("No slots available during DRBG instantiation.\n");
			break;
		case DRBG_RESULT(DRBG_ENTROPY_FAIL):
			printf("Entropy function error during DRBG instantiation.\n");
			break;
		default:
			printf("An error occurred during DRBG instantiation.\n");
			break;
		}
		return 1;
	}
	printf("DRBG at 256-bit security for key generation instantiated "
			"successfully.\n");

	/* Let's find out how large a buffer we need for the public and private
	 * keys.
	 */
	rc = ntru_crypto_ntru_encrypt_keygen(drbg, NTRU_EES1499EP1, &public_key_len,
	NULL, &private_key_len, NULL);
	if (rc != NTRU_OK)
		/* An error occurred requesting the buffer sizes needed. */
		return 1;
	printf("Public-key buffer size required: %d octets.\n", public_key_len);
	printf("Private-key buffer size required: %d octets.\n", private_key_len);

	public_key = malloc(sizeof(uint8_t) * public_key_len);
	private_key = malloc(sizeof(uint8_t) * private_key_len);

	/* Generate a key pair for EES401EP2.
	 * We must set the public-key length to the size of the buffer we have
	 * for the public key, and similarly for the private-key length.
	 * We've already done this by getting the sizes from the previous call
	 * to ntru_crypto_ntru_encrypt_keygen() above.
	 */
	uint16_t expected_private_key_len = private_key_len;
	rc = ntru_crypto_ntru_encrypt_keygen(drbg, NTRU_EES1499EP1, &public_key_len,
			public_key, &private_key_len, private_key);
	if (rc != NTRU_OK)
		/* An error occurred during key generation. */
		error = TRUE;
	if (expected_private_key_len != private_key_len) {
		fprintf(stderr, "private-key-length is different than expected\n");
		error = TRUE;
	}
	printf("Key-pair for NTRU_EES1499EP1 generated successfully.\n");

	/* Uninstantiate the DRBG. */
	rc = ntru_crypto_drbg_uninstantiate(drbg);
	if ((rc != DRBG_OK) || error) {
		printf("An error occurred uninstantiating the DRBG, or generating "
				"keys.\n");
		return 1;
	}
	printf("Key-generation DRBG uninstantiated successfully.\n");

	/* Writing both private key and public key to files */
	Handle = fopen("ntru-key.raw", "wb");
	if (Handle != NULL) {
		printf("Writing private key to ntru-key.raw\n");
		fwrite(private_key, private_key_len, 1, Handle);
		fclose(Handle);
	}

	Handle = fopen("ntru-pubkey.raw", "wb");
	if (Handle != NULL) {
		printf("Writing public key to ntru-pubkey.raw\n");
		fwrite(public_key, public_key_len, 1, Handle);
		fclose(Handle);
	}

	/* Let's find out how large a buffer we need for holding a DER-encoding
	 * of the public key.
	 */
	rc = ntru_crypto_ntru_encrypt_publicKey2SubjectPublicKeyInfo(public_key_len,
			public_key, &encoded_public_key_len, NULL);
	if (rc != NTRU_OK) {
		printf("An error occurred requesting the buffer size needed.\n");
		return 1;
	}
	printf("DER-encoded public-key buffer size required: %d octets.\n",
			encoded_public_key_len);

	encoded_public_key = malloc(sizeof(uint8_t) * encoded_public_key_len);
	uint16_t expected_encoded_public_key_len = encoded_public_key_len;

	/* DER-encode the public key for inclusion in a certificate.
	 * This creates a SubjectPublicKeyInfo field from a public key.
	 * We must set the encoded public-key length to the size of the buffer
	 * we have for the encoded public key.
	 * We've already done this by getting the size from the previous call
	 * to ntru_crypto_ntru_encrypt_publicKey2SubjectPublicKey() above.
	 */
	rc = ntru_crypto_ntru_encrypt_publicKey2SubjectPublicKeyInfo(public_key_len,
			public_key, &encoded_public_key_len, encoded_public_key);

	if (expected_encoded_public_key_len != encoded_public_key_len) {
		fprintf(stderr, "encoded_public_key_len is different than expected\n");
		error = TRUE;
	}

	printf("Public key DER-encoded for SubjectPublicKeyInfo successfully.\n");

	printf("DER encoded public key in hex:\n");
	DumpHex(encoded_public_key, encoded_public_key_len);

	Handle = fopen("ntru-pubkey.der", "wb");
	if (Handle != NULL) {
		printf("Writing DER encoded public key to ntru-pubkey.der\n");
		fwrite(encoded_public_key, encoded_public_key_len, 1, Handle);
		fclose(Handle);
	}

	/* Now suppose we are parsing a certificate so we can use the
	 * public key it contains, and the next field is the SubjectPublicKeyInfo
	 * field.  This is indicated by the "next" pointer.  We'll decode this
	 * field to retrieve the public key so we can use it for encryption.
	 * First let's find out how large a buffer we need for holding the
	 * DER-decoded public key.
	 */
	next = encoded_public_key; /* the next pointer will be pointing
	 to the SubjectPublicKeyInfo field */
	rc = ntru_crypto_ntru_encrypt_subjectPublicKeyInfo2PublicKey(next,
			&public_key_len, NULL, &next);
	if (rc != NTRU_OK) {
		printf("An error occurred requesting the buffer size needed.\n");
		return 1;
	}
	printf("Public-key buffer size required: %d octets.\n", public_key_len);

	/* Now we could allocate a buffer of length public_key_len to hold the
	 * decoded public key, but in this example we already have it as a
	 * local variable.
	 */

	/* Decode the SubjectPublicKeyInfo field.  Note that if successful,
	 * the "next" pointer will now point to the next field following
	 * the SubjectPublicKeyInfo field.
	 */
	rc = ntru_crypto_ntru_encrypt_subjectPublicKeyInfo2PublicKey(next,
			&public_key_len, public_key, &next);
	if (rc != NTRU_OK) {
		printf("An error occurred decoding the SubjectPublicKeyInfo field.\n"
				"This could indicate that the field is not a valid encoding"
				"of an NTRUEncrypt public key.\n");
		return 1;
	}
	printf("Public key decoded from SubjectPublicKeyInfo successfully.\n");

	return 0;
}
